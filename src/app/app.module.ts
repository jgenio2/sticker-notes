import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LabelsFilterComponent } from './components/labels-filter/labels-filter.component';
import { NotesBoardComponent } from './components/notes-board/notes-board.component';
import { NotesFilterComponent } from './components/notes-filter/notes-filter.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { StickerNoteComponent } from './components/sticker-note/sticker-note.component';
import { LabelComponent } from './components/tag/label.component';
import { EditLabelsDialogComponent } from './dialogs/edit-labels-dialog/edit-labels-dialog.component';
import { NoteDialogComponent } from './dialogs/note-dialog/note-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';

/**
 * AoT requires an exported function for factories
 * @param http HTTP client
 * @returns returns TranslateHttpLoader
 */
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    LabelsFilterComponent,
    NotesBoardComponent,
    NotesFilterComponent,
    StickerNoteComponent,
    LabelComponent,
    EditLabelsDialogComponent,
    NoteDialogComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatListModule,
    MatInputModule,
    MatButtonModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatIconModule,
    MatMenuModule,
    MatFormFieldModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

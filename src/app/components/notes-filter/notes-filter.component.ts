import { Component, OnDestroy, OnInit } from '@angular/core';
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';
import {
  Subject,
  Subscription,
  debounceTime,
  distinctUntilChanged,
} from 'rxjs';
import { NotesStoreService } from '../../services/notes-store.service';

/** Component to display the search bar */
@Component({
  selector: 'app-notes-filter',
  templateUrl: './notes-filter.component.html',
  styleUrls: ['./notes-filter.component.sass'],
})
export class NotesFilterComponent implements OnInit, OnDestroy {
  private readonly searchSubject = new Subject<string | undefined>();
  private searchSubscription?: Subscription;
  faMagnifyingGlass = faMagnifyingGlass;
  searchInput = '';

  /**
   * Instantiates the search bar.
   * @param {NotesStoreService} notesStoreService - Store to update list of notes
   */
  constructor(private notesStoreService: NotesStoreService) {}

  /**
   * Subscribes the search component.
   */
  public ngOnInit(): void {
    this.searchSubscription = this.searchSubject
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe(searchQuery =>
        this.notesStoreService.setNoteFilter(searchQuery || '')
      );
  }

  /**
   * Emits new value when search bar value is changed.
   */
  onSearchChange() {
    this.searchSubject.next(this.searchInput?.trim());
  }

  /**
   * Unsubscribe all subscriptions
   */
  public ngOnDestroy(): void {
    this.searchSubscription?.unsubscribe();
  }
}

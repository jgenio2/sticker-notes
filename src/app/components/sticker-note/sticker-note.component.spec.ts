import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { StickerNoteComponent } from './sticker-note.component';
import { INote, ITask } from '../../models/note.model';
import { NotesStoreService } from '../../services/notes-store.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('StickerNoteComponent', () => {
  let component: StickerNoteComponent;
  let fixture: ComponentFixture<StickerNoteComponent>;
  let notesStoreServiceMock: Partial<NotesStoreService>;
  let matDialogMock: Partial<MatDialog>;

  beforeEach(async () => {
    notesStoreServiceMock = {
      setNote: jest.fn(),
    };

    matDialogMock = {
      open: jest.fn(),
    };

    await TestBed.configureTestingModule({
      declarations: [StickerNoteComponent],
      imports: [
        FontAwesomeModule,
        MatCheckboxModule,
        MatCardModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [
        { provide: NotesStoreService, useValue: notesStoreServiceMock },
        { provide: MatDialog, useValue: matDialogMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(StickerNoteComponent);
    component = fixture.componentInstance;
    component.note = {
      id: 1,
      description: 'desc',
      title: 'Test Note',
      tasks: [{ id: 1, title: 'Task 1', done: false }],
      labels: [],
    };
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should update note when checkbox is changed', () => {
    const updatedNote: INote = {
      ...component.note,
      tasks: [{ id: 1, title: 'Task 1', done: false }],
    };
    component.onCheckboxChange(updatedNote);
    expect(notesStoreServiceMock.setNote).toHaveBeenCalledWith(updatedNote);
  });

  it('should open note dialog', () => {
    component.openNoteDialog();
    expect(matDialogMock.open).toHaveBeenCalledWith(expect.anything(), {
      data: component.note,
    });
  });

  it('should track task by id', () => {
    const task: ITask = { id: 1, title: 'Task 1', done: false };
    const result = component.trackByTaskId(0, task);
    expect(result).toBe(1);
  });
});

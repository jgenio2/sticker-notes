import { Component, Input } from '@angular/core';
import { INote, ITask } from '../../models/note.model';
import { NotesStoreService } from '../../services/notes-store.service';
import { faPenToSquare } from '@fortawesome/free-solid-svg-icons';
import { NoteDialogComponent } from '../../dialogs/note-dialog/note-dialog.component';
import { MatDialog } from '@angular/material/dialog';

/** Component to display a note */
@Component({
  selector: 'app-sticker-note',
  templateUrl: './sticker-note.component.html',
  styleUrls: ['./sticker-note.component.sass'],
})
export class StickerNoteComponent {
  @Input()
  note!: INote;
  faPenToSquare = faPenToSquare;

  /**
   * Instantiates a new note.
   * @param {NotesStoreService} notesStoreService - Store to update list of notes
   * @param {MatDialog} dialog - Reference to MatDialog class
   */
  constructor(
    private notesStoreService: NotesStoreService,
    public dialog: MatDialog
  ) {}

  /**
   * Updates task when checking/unchecking checkbox.
   * @param {INote} note - note to be updated
   */
  onCheckboxChange(note: INote): void {
    this.notesStoreService.setNote(note);
  }

  /**
   * Opens dialog to edit note.
   */
  openNoteDialog(): void {
    this.dialog.open(NoteDialogComponent, {
      data: this.note,
    });
  }

  /**
   * Tracks task by id(to be used by ngFor)
   * @param {number} index - task DOM index
   * @param {ITask} task - task
   * @returns returns task id
   */
  trackByTaskId(index: number, task: ITask): number {
    return task.id;
  }
}

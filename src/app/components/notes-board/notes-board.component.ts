import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { INote } from '../../models/note.model';

/** Component to display the board of notes */
@Component({
  selector: 'app-notes-board',
  templateUrl: './notes-board.component.html',
  styleUrls: ['./notes-board.component.sass'],
})
export class NotesBoardComponent implements OnInit, OnDestroy {
  @Input()
  notes$!: Observable<INote[]>;
  columns$: BehaviorSubject<INote[][]> = new BehaviorSubject<INote[][]>([]);
  subscription!: Subscription;

  /**
   * Subscribes the list of notes to create the data struture in order to display them accordingly to the mockups.
   */
  ngOnInit() {
    this.subscription = this.notes$.subscribe(notes => {
      let colNumber = 0;
      const columns: INote[][] = [[], [], [], []];
      notes.forEach(note => {
        columns[colNumber].push(note);
        colNumber = colNumber > 2 ? 0 : colNumber + 1;
      });
      this.columns$.next(columns);
    });
  }

  /**
   * Tracks note by id(to be used by ngFor)
   * @param {number} index - note DOM index
   * @param {INote} note - note
   * @returns returns note id
   */
  trackByNoteId(index: number, note: INote): number {
    return note.id;
  }

  /**
   * Unsubscribe all subscriptions
   */
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

import { Component, Input, OnInit } from '@angular/core';

/** Component to display labels */
@Component({
  selector: 'app-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.sass'],
})
export class LabelComponent {
  @Input()
  label: string = '';
}

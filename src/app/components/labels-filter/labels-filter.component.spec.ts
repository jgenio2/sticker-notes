import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LabelsFilterComponent } from './labels-filter.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../../app.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { NotesStoreService } from '../../services/notes-store.service';

describe('LabelsFilterComponent', () => {
  let component: LabelsFilterComponent;
  let fixture: ComponentFixture<LabelsFilterComponent>;
  let notesStoreServiceMock: Partial<NotesStoreService>;
  let matDialogMock: Partial<MatDialog>;

  beforeEach(async () => {
    notesStoreServiceMock = {
      setNote: jest.fn(),
    };

    matDialogMock = {
      open: jest.fn(),
    };

    await TestBed.configureTestingModule({
      declarations: [LabelsFilterComponent],
      imports: [
        HttpClientModule,
        FontAwesomeModule,
        TranslateModule.forRoot({
          defaultLanguage: 'en',
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient],
          },
        }),
      ],
      providers: [
        { provide: NotesStoreService, useValue: notesStoreServiceMock },
        { provide: MatDialog, useValue: matDialogMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(LabelsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

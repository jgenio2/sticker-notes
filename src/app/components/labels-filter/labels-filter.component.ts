import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import {
  faTag,
  faPenToSquare,
  faPencil,
} from '@fortawesome/free-solid-svg-icons';
import { EditLabelsDialogComponent } from '../../dialogs/edit-labels-dialog/edit-labels-dialog.component';
import { NotesStoreService } from '../../services/notes-store.service';

/** Component to display the filter of labels */
@Component({
  selector: 'app-labels-filter',
  templateUrl: './labels-filter.component.html',
  styleUrls: ['./labels-filter.component.sass'],
})
export class LabelsFilterComponent {
  @Input()
  labels$!: BehaviorSubject<string[]>;
  faTag = faTag;
  faPenToSquare = faPenToSquare;
  faPencil = faPencil;
  enabledLabel: string = '';

  /**
   * Instantiates the filter of labels.
   * @param {MatDialog} dialog - Reference to MatDialog class
   * @param {NotesStoreService} notesStoreService - Store to update list of notes
   */
  constructor(
    public dialog: MatDialog,
    private notesStoreService: NotesStoreService
  ) {}

  /**
   * Called when label is clicked.
   * @param {string} label - label to be filtered by
   */
  onLabelClick(label: string) {
    this.enabledLabel = label === this.enabledLabel ? '' : label;
    this.notesStoreService.setLabelFilter(this.enabledLabel);
  }

  /**
   * Checks if label is selected.
   * @param {string} label - label to be filtered by
   */
  isLabelSelected(label: string) {
    return this.enabledLabel === label;
  }

  /**
   * Opens dialog to edit note.
   */
  openDialog() {
    this.dialog.open(EditLabelsDialogComponent, {
      data: this.labels$.getValue(),
    });
  }
}

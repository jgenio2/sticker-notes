export interface ITask {
  id: number;
  title: string;
  done: boolean;
}

export interface INote {
  id: number;
  title: string;
  description: string;
  tasks: ITask[];
  labels: string[];
}

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { faXmark, faTrash } from '@fortawesome/free-solid-svg-icons';
import { NotesStoreService } from '../../services/notes-store.service';

/** Component to edit labels */
@Component({
  selector: 'app-edit-labels-dialog',
  templateUrl: './edit-labels-dialog.component.html',
  styleUrls: ['./edit-labels-dialog.component.sass'],
})
export class EditLabelsDialogComponent {
  faXmark = faXmark;
  faTrash = faTrash;
  labelToAdd = '';

  /**
   * Instantiates Edit labels Dialog.
   * @param {string[]} labels - Reference to dialog
   * @param {NotesStoreService} notesStoreService - Store to update list of notes
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public labels: string[],
    private notesStoreService: NotesStoreService
  ) {}

  /**
   * Deletes label.
   * @param {string} label - Label to delete
   */
  deleteLabel(label: string) {
    this.labels = this.notesStoreService.removeLabel(label);
  }

  /**
   * Adds a new label.
   */
  createLabel() {
    this.labels = this.notesStoreService.addLabel(this.labelToAdd);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EditLabelsDialogComponent } from './edit-labels-dialog.component';
import { NotesStoreService } from '../../services/notes-store.service';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpLoaderFactory } from '../../app.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';

describe('EditLabelsDialogComponent', () => {
  let component: EditLabelsDialogComponent;
  let fixture: ComponentFixture<EditLabelsDialogComponent>;
  let notesStoreServiceMock: jest.Mocked<NotesStoreService>; // Use jest.Mocked to mock methods

  beforeEach(async () => {
    notesStoreServiceMock = {
      removeLabel: jest.fn(),
      addLabel: jest.fn(),
    } as unknown as jest.Mocked<NotesStoreService>; // Type cast to jest.Mocked to enable jest.fn()

    await TestBed.configureTestingModule({
      declarations: [EditLabelsDialogComponent],
      imports: [
        FontAwesomeModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        TranslateModule.forRoot({
          defaultLanguage: 'en',
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient],
          },
        }),
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: [] },
        { provide: NotesStoreService, useValue: notesStoreServiceMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EditLabelsDialogComponent);
    component = fixture.componentInstance;
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should delete label', () => {
    const labelToDelete = 'TestLabel';
    component.labels = ['TestLabel', 'AnotherLabel'];
    notesStoreServiceMock.removeLabel.mockReturnValue(['AnotherLabel']);

    component.deleteLabel(labelToDelete);

    expect(notesStoreServiceMock.removeLabel).toHaveBeenCalledWith(
      labelToDelete
    );
    expect(component.labels).toEqual(['AnotherLabel']);
  });

  it('should create label', () => {
    component.labelToAdd = 'NewLabel';
    component.labels = ['ExistingLabel'];
    notesStoreServiceMock.addLabel.mockReturnValue([
      'ExistingLabel',
      'NewLabel',
    ]);

    component.createLabel();

    expect(notesStoreServiceMock.addLabel).toHaveBeenCalledWith('NewLabel');
    expect(component.labels).toEqual(['ExistingLabel', 'NewLabel']);
  });
});

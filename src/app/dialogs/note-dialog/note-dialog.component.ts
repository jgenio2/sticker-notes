import { Component, Inject, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {
  faPlus,
  faTag,
  faTrash,
  faXmark,
} from '@fortawesome/free-solid-svg-icons';
import { BehaviorSubject, Subscription } from 'rxjs';
import { INote, ITask } from '../../models/note.model';
import { NotesStoreService } from '../../services/notes-store.service';

export interface LabelCheckbox {
  label: string;
  isSelected: boolean;
}

/** Component to create/edit notes */
@Component({
  selector: 'app-note-dialog',
  templateUrl: './note-dialog.component.html',
  styleUrls: ['./note-dialog.component.sass'],
})
export class NoteDialogComponent implements OnDestroy {
  faXmark = faXmark;
  faPlus = faPlus;
  faTag = faTag;
  faTrash = faTrash;
  taskToAdd = '';
  labels = this.notesStoreService.getLabels();
  labelCheckboxes = new BehaviorSubject<LabelCheckbox[]>([]);
  labelSubscription: Subscription;
  noteForm: FormGroup;

  /**
   * Setups validators and labels checkboxes.
   * @param {MatDialogRef<NoteDialogComponent>} dialogRef - Reference to dialog
   * @param {INote} note - note to edit
   * @param {NotesStoreService} notesStoreService - Store to update list of notes
   */
  constructor(
    private dialogRef: MatDialogRef<NoteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public note: INote,
    private notesStoreService: NotesStoreService
  ) {
    this.noteForm = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50),
      ]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50),
      ]),
    });

    this.labelSubscription = this.labels.subscribe(labels => {
      this.labelCheckboxes.next(
        labels.map(label => {
          return {
            label: label,
            isSelected: note.labels.includes(label),
          };
        })
      );
    });
  }

  /**
   * Adds new task to current note.
   * @param {string} taskToAdd - Task name
   */
  addNewTask(taskToAdd: string) {
    if (
      taskToAdd &&
      (!this.note.tasks ||
        this.note.tasks?.every(task => task.title !== taskToAdd))
    ) {
      this.note.tasks.push({
        id: this.note.tasks.length,
        title: this.taskToAdd,
        done: false,
      });
    }
  }

  /**
   * Updates labels list of the current note.
   * @param {LabelCheckbox} checkbox - label checkbox model
   */
  onCheckboxChange(checkbox: LabelCheckbox) {
    let labelsFromNote = this.note.labels;
    if (checkbox.isSelected && !labelsFromNote.includes(checkbox.label)) {
      labelsFromNote = [...labelsFromNote, checkbox.label];
    } else if (!checkbox.isSelected) {
      labelsFromNote = labelsFromNote.filter(label => label !== checkbox.label);
    }
    this.note.labels = labelsFromNote;
  }

  /**
   * Removes task from the current note.
   * @param {Event} evt - click event
   * @param {ITask} taskToRemove - task to be removed from current note
   */
  deleteTask(evt: Event, taskToRemove: ITask) {
    evt.stopPropagation();
    this.note.tasks = this.note.tasks.filter(task => task !== taskToRemove);
  }

  /**
   * Removes note from notes store.
   */
  deleteNote() {
    this.notesStoreService.removeNote(this.note);
  }

  /**
   * Submit button callback.
   */
  confirmChanges() {
    if (this.isFormInvalid()) return;

    this.notesStoreService.setNote(this.note);
    this.dialogRef.close();
  }

  /**
   * Checks if submit button is disabled
   * @returns returns true if form is invalid
   */
  isFormInvalid() {
    return this.noteForm.invalid || !this.note.labels?.length;
  }

  /**
   * Tracks by id(to be used by ngFor)
   * @param {number} index - task DOM index
   * @param {ITask} task - task
   * @returns returns task id
   */
  trackByTaskId(index: number, task: ITask): number {
    return task.id;
  }

  /**
   * Unsubscribe all subscriptions
   */
  ngOnDestroy(): void {
    this.labelSubscription.unsubscribe();
  }
}

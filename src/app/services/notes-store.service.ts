import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
import { INote } from '../models/note.model';

/** Store of notes and labels */
@Injectable({ providedIn: 'root' })
export class NotesStoreService {
  private notes$ = new BehaviorSubject<INote[]>([]);
  private filteredNotes$ = new BehaviorSubject<INote[]>([]);
  private labels$ = new BehaviorSubject<string[]>([]);
  private labelFilter$ = new BehaviorSubject<string>('');
  private noteFilter$ = new BehaviorSubject<string>('');

  /**
   * Gets subject of notes
   * @returns returns subject of notes
   */
  getNotes(): BehaviorSubject<INote[]> {
    return this.notes$;
  }

  /**
   * Gets subject of filtered notes
   * @returns returns subject of filtered notes
   */
  getFilteredNotes(): BehaviorSubject<INote[]> {
    return this.filteredNotes$;
  }

  /**
   * Sets a new list of notes and updates the displayed notes
   * @param notes updated list of notes
   */
  setNotes(notes: INote[]): void {
    this.notes$.next(notes);
    localStorage.setItem('notes', JSON.stringify(this.notes$.getValue()));
    this.updateFilteredNotes();
  }

  /**
   * Adds a new note or updates an existing note and updates the displayed notes
   * @param note updated note
   */
  setNote(note: INote): void {
    const notesList = this.notes$.getValue();
    if (!notesList.find(curNote => note.id === curNote.id)) {
      this.notes$.next([...notesList, note]);
    } else {
      this.notes$.next(
        notesList.map(curNote => (note.id === curNote.id ? note : curNote))
      );
    }

    localStorage.setItem('notes', JSON.stringify(this.notes$.getValue()));
    this.updateFilteredNotes();
  }

  /**
   * Remove a note from the notes list
   * @param note note to remove
   */
  removeNote(note: INote): void {
    this.setNotes(
      this.notes$.getValue().filter(curNote => curNote.id !== note.id)
    );
  }

  /**
   * Updates displayed notes list
   */
  updateFilteredNotes(): void {
    const labelFilter = this.labelFilter$.getValue();
    const noteFilter = this.noteFilter$.getValue().toLowerCase();
    let filteredNotes = labelFilter
      ? this.notes$.getValue().filter(note => note.labels.includes(labelFilter))
      : this.notes$.getValue();

    filteredNotes = noteFilter
      ? filteredNotes.filter(note =>
          note.title?.toLowerCase().includes(noteFilter)
        )
      : filteredNotes;

    this.filteredNotes$.next(filteredNotes);
  }

  /**
   * Gets subject of labels
   * @returns returns subject of labels
   */
  getLabels(): BehaviorSubject<string[]> {
    return this.labels$;
  }

  /**
   * Updates the tag filter
   * @param label tag to filter on
   */
  setLabelFilter(label: string): void {
    this.labelFilter$.next(label);
    this.updateFilteredNotes();
  }

  /**
   * Updates the note filter
   * @param text title to filter on
   */
  setNoteFilter(text: string): void {
    this.noteFilter$.next(text);
    this.updateFilteredNotes();
  }

  /**
   * Updates the labels list
   * @param labels updated list of labels
   */
  setLabels(labels: string[]): void {
    this.labels$.next(labels);
    localStorage.setItem('labels', JSON.stringify(this.labels$.getValue()));
  }

  /**
   * Adds a new label to the list of labels
   * @param label new label to added
   * @returns returns updated list of labels
   */
  addLabel(label: string): string[] {
    let labelsList = this.labels$.getValue();
    if (label && !labelsList.includes(label)) {
      labelsList = [...labelsList, label];
      this.setLabels(labelsList);
    }

    return labelsList;
  }

  /**
   * Removes a label to the list of labels and removes the notes with no labels
   * @param label new label to removed
   * @returns returns updated list of labels
   */
  removeLabel(label: string): string[] {
    let updatedNotes = this.notes$.getValue().map(curNote => {
      if (curNote.labels.includes(label)) {
        curNote.labels = curNote.labels.filter(curLabel => curLabel !== label);
      }
      return curNote;
    });

    updatedNotes = updatedNotes.filter(note => note.labels.length !== 0);
    this.setNotes(updatedNotes);
    const updatedLabels = this.labels$
      .getValue()
      .filter(curLabel => curLabel !== label);
    this.setLabels(updatedLabels);

    return updatedLabels;
  }
}

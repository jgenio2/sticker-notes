import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { throwError, of } from 'rxjs';
import { NotesApiService } from './notes-api.service';
import { INote } from '../models/note.model';

describe('NotesApiService', () => {
  let service: NotesApiService;
  let httpClientSpy: { get: jest.Mock };

  beforeEach(() => {
    httpClientSpy = { get: jest.fn() };

    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: httpClientSpy }],
    });

    service = TestBed.inject(NotesApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve notes from the API', () => {
    const mockNotes: INote[] = [
      { id: 1, title: 'Note 1', description: '', tasks: [], labels: [] },
    ];
    httpClientSpy.get.mockReturnValue(of(mockNotes));

    service.retrieveNotes().subscribe(notes => {
      expect(notes).toEqual(mockNotes);
    });

    expect(httpClientSpy.get).toHaveBeenCalledWith('/notes');
  });

  it('should handle error while retrieving notes from the API', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Error occurred',
      status: 500,
    });
    httpClientSpy.get.mockReturnValue(throwError(errorResponse));

    service.retrieveNotes().subscribe(
      () => {},
      error => {
        expect(error).toEqual(errorResponse);
      }
    );

    expect(httpClientSpy.get).toHaveBeenCalledWith('/notes');
  });

  it('should retrieve labels from the API', () => {
    const mockLabels: string[] = ['label1', 'label2'];
    httpClientSpy.get.mockReturnValue(of(mockLabels));

    service.retrieveLabels().subscribe(labels => {
      expect(labels).toEqual(mockLabels);
    });

    expect(httpClientSpy.get).toHaveBeenCalledWith('/labels');
  });

  it('should handle error while retrieving labels from the API', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Error occurred',
      status: 500,
    });
    httpClientSpy.get.mockReturnValue(throwError(errorResponse));

    service.retrieveLabels().subscribe(
      () => {},
      error => {
        expect(error).toEqual(errorResponse);
      }
    );

    expect(httpClientSpy.get).toHaveBeenCalledWith('/labels');
  });
});

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, catchError, throwError } from 'rxjs';
import { INote } from '../models/note.model';

/** Service to retrieve notes and labels */
@Injectable({ providedIn: 'root' })
export class NotesApiService {
  /**
   * Instantiate class to call API.
   * @param {HttpClient} http - Service to call API
   */
  constructor(private http: HttpClient) {}

  /**
   * Gets notes from API
   * @returns returns Observable of notes
   */
  retrieveNotes(): Observable<INote[]> {
    return this.http.get<INote[]>('/notes').pipe(
      catchError(err => {
        console.error('Error while fetching notes', err);
        return throwError(() => err);
      })
    );
  }

  /**
   * Gets labels from API
   * @returns returns Observable of labels
   */
  retrieveLabels(): Observable<string[]> {
    return this.http.get<string[]>('/labels').pipe(
      catchError(err => {
        console.error('Error while fetching labels', err);
        return throwError(() => err);
      })
    );
  }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { NotesApiService } from './services/notes-api.service';
import { NotesStoreService } from './services/notes-store.service';
import { Subscription } from 'rxjs';
import { INote } from './models/note.model';
import { faCirclePlus } from '@fortawesome/free-solid-svg-icons';
import { NoteDialogComponent } from './dialogs/note-dialog/note-dialog.component';
import { MatDialog } from '@angular/material/dialog';

/** Root component */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent implements OnInit, OnDestroy {
  notesSubscription!: Subscription;
  labelsSubscription!: Subscription;
  notes$ = this.notesStoreService.getNotes();
  filteredNotes$ = this.notesStoreService.getFilteredNotes();
  labels$ = this.notesStoreService.getLabels();
  faCirclePlus = faCirclePlus;

  /**
   * Instantiates root class.
   * @param {NotesApiService} notesApiService - Service to call API
   * @param {NotesStoreService} notesStoreService - Store fo notes and labels
   * @param {MatDialog} dialog - MatDialog class
   */
  constructor(
    private notesApiService: NotesApiService,
    private notesStoreService: NotesStoreService,
    public dialog: MatDialog
  ) {}

  /**
   * Loads notes and labels
   */
  ngOnInit() {
    this.loadNotes();
    this.loadLabels();
  }

  /**
   * Retrieves notes from API/Local storage and loads them into the store
   */
  loadNotes() {
    const localNotes = localStorage.getItem('notes');
    if (localNotes === null) {
      this.notesSubscription = this.notesApiService
        .retrieveNotes()
        .subscribe((notes: INote[]) => this.notesStoreService.setNotes(notes));
    } else {
      this.notesStoreService.setNotes(JSON.parse(localNotes));
    }
  }

  /**
   * Retrieves labels from API/Local storage and loads them into the store
   */
  loadLabels() {
    const localLabels = localStorage.getItem('labels');
    if (localLabels === null) {
      this.labelsSubscription = this.notesApiService
        .retrieveLabels()
        .subscribe((labels: string[]) =>
          localStorage.setItem('labels', JSON.stringify(labels))
        );
    } else {
      this.labels$.next(JSON.parse(localLabels));
    }
  }

  /**
   * Opens the dialog to create a new note
   */
  openNoteDialog() {
    this.dialog.open(NoteDialogComponent, {
      data: {
        id: this.getNextId(),
        title: '',
        description: '',
        tasks: [],
        labels: [],
      },
    });
  }

  /**
   * Gets new UUID
   * @returns returns new UUID
   */
  getNextId() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(
      /[xy]/g,
      function (c) {
        const r = (Math.random() * 16) | 0,
          v = c == 'x' ? r : (r & 0x3) | 0x8;
        return v.toString(16);
      }
    );
  }

  /**
   * Unsubscribe all subscriptions
   */
  ngOnDestroy() {
    this.notesSubscription?.unsubscribe();
    this.labelsSubscription?.unsubscribe();
  }
}
